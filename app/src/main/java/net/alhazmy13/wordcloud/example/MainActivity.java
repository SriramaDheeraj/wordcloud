package net.alhazmy13.wordcloud.example;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import net.alhazmy13.example.R;
import net.alhazmy13.wordcloud.ColorTemplate;
import net.alhazmy13.wordcloud.WordCloud;
import net.alhazmy13.wordcloud.WordCloudView;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
   public Uri filePath;
   public String path = "";
    public String FileData = "";
    List<WordCloud> list ;
    public Button uploadFile, createWordCloud;
    private int REQUEST_CODE_DOC = 100;
    public WordCloudView wordCloud;
    String simpleText = "Hello! edjekdjnkjdejhkj ekdjekdjhedhf hello what's up and  hello! android app android studio word cloud generator from the text file";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        uploadFile = (Button) findViewById(R.id.button);
        createWordCloud = (Button) findViewById(R.id.button2);
        wordCloud = (WordCloudView) findViewById(R.id.wordCloud);
        //upload button onClick listener
        uploadFile.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Calling the getDocument function
            getDocument();

        }
    });
        // create button on click listener
    createWordCloud.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Calling the generateWordCloud() function
           generateWordCloud();

        }
    });
    }
    // Function to read the data in the text file
    private void generateWordCloud() {
        File myExternalFile = new File(path);
        try {
            FileInputStream fis = new FileInputStream(myExternalFile);
            DataInputStream in = new DataInputStream(fis);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            String strLine;
            while ((strLine = br.readLine()) != null) {
                FileData = FileData + strLine + "\n";
            }
            br.close();
            in.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // appending read data from the file to the string variable
        simpleText += FileData;
        // spiting the data in to words
        String[] splitData = simpleText.split(" ");
        // a list to add the data
        list = new ArrayList<>();
        Random random = new Random();
        for (String s : splitData) {
            list.add(new WordCloud(s,random.nextInt(50)));
        }
  //      Log.d("CREATION", Arrays.toString(splitData));
        // setting up the wordCloud with the created list
        wordCloud.setDataSet(list);
        wordCloud.setSize(200,200);
        wordCloud.setColors(ColorTemplate.MATERIAL_COLORS);
        wordCloud.notifyDataSetChanged();
    }

    // Function to upload a file
    private void getDocument()
    {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select File"),REQUEST_CODE_DOC);


    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_DOC && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                path = PdfFilePath.getPath(MainActivity.this, filePath);
            } catch (Exception ignored) {
                path = "";
            }

        }
    }

}





